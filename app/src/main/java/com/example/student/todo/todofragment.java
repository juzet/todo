package com.example.student.todo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class todofragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity mainActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback =  (ActivityCallback)activity;
        this.mainActivity = (MainActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        todoitem i1 = new todoitem("Sleep", "Wake Up", "Work", "Sleep");
        todoitem i2 = new todoitem("Notes", "Homework", "Quiz", "Test");
        todoitem i3 = new todoitem("Watch TV", "PLay Games", "Call", "Text");
        todoitem i4 = new todoitem("Jump", "Play", "Walk", "Skip");

        mainActivity.todoitem.add(i1);
        mainActivity.todoitem.add(i2);
        mainActivity.todoitem.add(i3);
        mainActivity.todoitem.add(i4);

        todoPostAdapter adapter = new todoPostAdapter(mainActivity.todoitem, activityCallback);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
