package com.example.student.todo;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemFragment extends Fragment{
    private MainActivity activity;
    public TextView tv1;
    public TextView tv2;
    public TextView tv3;
    public TextView tv4;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_item, container, false);
        tv1 = (TextView)layoutView.findViewById(R.id.todo1);
        tv2 = (TextView)layoutView.findViewById(R.id.todo2);
        tv3 = (TextView)layoutView.findViewById(R.id.todo3);
        tv4 = (TextView)layoutView.findViewById(R.id.todo4);

        tv1.setText("Name: " + activity.todoitem.get(activity.currentItem).title);
        tv2.setText("Name: " + activity.todoitem.get(activity.currentItem).dateAdded);
        tv3.setText("Name: " + activity.todoitem.get(activity.currentItem).dateDue);
        tv4.setText("Name: " + activity.todoitem.get(activity.currentItem).chores);

        return layoutView;
}


}
