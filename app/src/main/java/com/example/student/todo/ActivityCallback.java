package com.example.student.todo;

import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(int pos);
}
