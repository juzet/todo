package com.example.student.todo;

import android.net.Uri;
import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class todoPostAdapter extends RecyclerView.Adapter<todoPostHolder> {

    private ArrayList<todoitem> todoitem;
    private ActivityCallback activityCallback;

    public todoPostAdapter(ArrayList<todoitem> todoitem, ActivityCallback activityCallback) {
        this.activityCallback = activityCallback;
        this.todoitem = todoitem;
    }

    @Override
    public todoPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new todoPostHolder(view);
    }

    @Override
    public void onBindViewHolder(todoPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Uri redditUri = Uri.parse(redditPosts.get(position).url);
                activityCallback.onPostSelected(position);
            }
        });
        holder.titleText.setText(todoitem.get(position).title);

    }

    @Override
    public int getItemCount() {

        return todoitem.size();
    }
}